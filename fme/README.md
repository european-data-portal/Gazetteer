# European Data Portal - Gazetteer

## INTRODUCTION

A Gazetteer is a geographical dictionary that links place names to coordinates or the other way around to help users to locate and find data sets available in the regions of interest. Primary function of the Gazetteer is to expose the Open Government Data (OGD) that is available for a certain "Place" (e.g. a city, country, district …). It provides the spatial reference (e.g. points with X/Y coordinates) for a place, which is then used to link to the geospatial metadata from the OGD.  This "linking" through actual geospatial references is better than pure "textual" searches as they can be ambiguous or have multiple meanings (e.g. cities that have the same name). Besides supporting search a Gazetteer can also help to improve the quality of metadata that is inserted into the repository.

## Purpose and Scope

The objectives of this task are to integrate in the Open Data Portal a gazetteer functionality that will help users to find open data for certain places using geographical names spatial datasets provided by INSPIRE and other sources. This is according to data specification in version 3.198:

_"__Search referring to place names must be supported by the implementation of a gazetteer that shall make use of the INSPIRE annex I geographical names spatial datasets in the Member States12. Integration with other place identifiers from extensively used linked data resources will be positively evaluated. The gazetteer functionality must support local caching as well as real time remote access to Member States geographical names spatial datasets."_

This search is user friendly, intuitive, with good performance and integrated into the web environment using the RESTful endpoint of the gazetteer. The consortium implemented the gazetteer with the FME (Feature Manipulation Engine) to support remote access to Member states geographical names spatial datasets and to fill a local cache, an Apache Solr Lucene Index. The gazetteer provides information for data search for locations in different languages. For example a user in the Netherlands will be able to search for data in the German capital Berlin by typing its Dutch name "Berlijn", a French speaking person will be able to search data for London by typing in "Londres".

## Update Notes
In a recent update, geonames made minor changes to their database model. Therefore, local tables have to be recreated. Local tables can be dropped using the workspace dropLocalTables.fmw. During the process, the new database model will be deployed automatically. 


## Installation Guide
### Gazetteer Harvesting – FME Server

- Install FME Server
    - Relevant FME Server documentation:
        -   [http://docs.safe.com/fme/html/FME\_Server\_Documentation/Default.htm#AdminGuide/Before\_You\_Begin.htm](http://docs.safe.com/fme/html/FME_Server_Documentation/Default.htm#AdminGuide/Before_You_Begin.htm)
        -   [http://docs.safe.com/fme/html/FME\_Server\_Documentation/Default.htm#AdminGuide/Configuring\_FME\_Server\_to\_Send\_Email.htm](http://docs.safe.com/fme/html/FME_Server_Documentation/Default.htm#AdminGuide/Configuring_FME_Server_to_Send_Email.htm)
        -  [http://docs.safe.com/fme/html/FME\_Server\_Documentation/Default.htm#WebUI/schedules.htm?Highlight=schedule](http://docs.safe.com/fme/html/FME_Server_Documentation/Default.htm#WebUI/schedules.htm?Highlight=schedule)
        -  [http://docs.safe.com/fme/html/FME\_Server\_Documentation/Default.htm#WebUI/Cleanup.htm](http://docs.safe.com/fme/html/FME_Server_Documentation/Default.htm#WebUI/Cleanup.htm)
- Install PostgreSQL with PostGIS extension (default installation)
- Configure FME Server
    - Create a Topic called "odp-failure" and configure a Subscription to send an Email
    - The email configured will be sent whenever an error occurs during the harvesting processes
- Publish all FME Workspaces via FME Workbench
- Create a Scheduled Process
    - Select "serverJobSubmitter.fmw" as a main Workspace
    - Fill in your database credentials
    - Fill in your FME Server credentials
    - Fill in the Solr URL used to swap your cores
    - Fill in the Solr URL to delete your background core
    - Fill in the Solr credentials
- Configure Cleanup Tasks
- For the merging process as PostgreSQL/ PostGIS database has to be existing
    - SQL Statement:
    "CREATE DATABASE gazetteer; CREATE EXTENSION postgis;"

#### Logging

All FME processes create a log file which can be accessed via the web interface of the FME Server. The log files will be removed after a predefined time which can be configured within the FME Cleanup Tasks (see previous chapter).

### Troubleshooting

What can I do if the REST interface is not available?

- Make sure that the Solr is deployed correctly and that the service is running

A FME constantly fails:

- Please read the log file of the process to get detailed information about the error.
- Common problems could be:
  - o.The PostGIS database is not accessible
  - o.The source to be harvested is not available or invalid
  - o.The Solr endpoint can't be accessed

Gazetteer entries of a country are missing:

- Check if the corresponding FME job has succeeded
- If the job was successful but didn't produce any outputs, check the original data source

## Infrastructure specifications

### Solr

The system requirements for the Solr can be found [here](https://solr.apache.org/guide/8_8/solr-system-requirements.html)

### FME Server

Technical Requirements for the FME Server can be found on the official Safe Software website:
  [http://www.safe.com/fme/fme-server/tech-specs/](http://www.safe.com/fme/fme-server/tech-specs/)
  
## Obtaining a FME License

In order to use this software component you need a license for [FME](https://www.con-terra.com/portfolio/fme).
Evaluation licenses will be available on request. For further information, please get in touch with:

* [con terra EDP team](mailto:edp@conterra.de)
